#!/bin/env python3
import argparse
import configparser
import getpass
import ipaddress
import json
import logging
import os
import subprocess
import sys

import jinja2

import re2oapi


path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger('dhcp')
handler = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		description="Generate DHCP",
	)
	parser.add_argument("-e", "--export", help="Exporte la configuration des dhcps sur la sortie standard", action="store_true")
	parser.add_argument("-p", "--re2o-password", help="Demande le mot de passe de l'utilisateur re2o", action="store_true")
	parser.add_argument("-q", "--quiet", help="Diminue la verbosité des logs (à spécifier plusieurs fois pour diminuer la verbosité)", action='count', default=0)
	parser.add_argument("-r", "--re2o-server", help="Nom du serveur re2o à contacter", type=str, default=None)
	parser.add_argument("-u", "--re2o-user", help="Utilisateur re2o", type=str, default=None)
	parser.add_argument("-d", "--do-not-regen", help="Do not regen the mail server", action='store_true')
	parser.add_argument("-v", "--verbose", help="Augmente la verbosité des logs (à spécifier plusieurs fois pour augmenter la verbosité)", action='count', default=0)
	args = parser.parse_args()

	verbosity = args.verbose - args.quiet

	if verbosity <= -1:
		logger.setLevel(logging.WARNING)
	elif verbosity == 0:
		logger.setLevel(logging.INFO)
	elif verbosity >= 1:
		logger.setLevel(logging.DEBUG)

	logger.info("Reading configuration")
	with open(os.path.join(path, "dhcp.json")) as config_file:
		config = json.load(config_file)
	logger.debug("Loaded {}".format(config))

	logger.info("Reading Re2o configuration")
	re2o_config = configparser.ConfigParser()
	re2o_config.read(os.path.join(path, 're2o-config.ini'))

	if args.re2o_server is not None:
		api_hostname = args.re2o_server
	else:
		api_hostname = re2o_config.get('Re2o', 'hostname')
	if args.re2o_user is not None:
		api_username = args.re2o_user
	else:
		api_username = re2o_config.get('Re2o', 'username')
	if args.re2o_password:
		api_password = getpass.getpass('Re2o password: ')
	else:
		api_password = re2o_config.get('Re2o', 'password')

	logger.info("Connecting to Re2o")
	api_client = re2oapi.Re2oAPIClient(api_hostname, api_username, api_password, use_tls=False)

	logger.info("Querying Re2o")
	machines_api = api_client.list("dhcp/hostmacip")

	machines_pool = { extension : [] for extension in config['extensions'] }

	for machine in machines_api:
		if machine['extension'][1:] not in config['extensions'] or \
			any(key not in machine.keys() for key in ['ipv4', 'mac_address', 'hostname']) :
			continue
		extension = machine['extension'][1:]
		machines_pool[extension].append(
			{
				'hostname': machine['hostname'],
				'mac': machine['mac_address'],
				'ip': machine['ipv4']
			}
		)

	for extension, machines in machines_pool.items():
		with open(os.path.join(path, 'templates', 'list.j2')) as list_template:
			template = jinja2.Template(list_template.read())

		if args.export:
			print('#'*(22+len(extension)))
			print(f'########## {extension} ##########')
			print('#'*(22+len(extension)))
			print(template.render(machines=machines, extension=extension))
		else:
			with open(os.path.join(path, 'generated', f"dhcp.{extension}.list"), 'w') as generated:
				generated.write(template.render(machines=machines, extension=extension))
			if not args.do_not_regen and subprocess.run(['/usr/bin/systemctl', 'is-active', '--quiet', 'isc-dhcp-server']).returncode == 0:
				subprocess.run(['/usr/bin/systemctl', 'restart', 'isc-dhcp-server'])
